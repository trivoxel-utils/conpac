## Created by: Caden Mitchell <https://trivoxel.io>

MAINTAINER=		tri.voxel@gmail.com
COMMENT=			Universal package management tool
LICENSE=			MIT

SOFTNAME=			conpac
CATEGORIES=		ports-mgmt
MASTER_SITES=	https://gitlab.com/trivoxel/${NAME}
PREFIX=				/usr/local

default_target: help
.PHONY: default_target install uninstall update

install:
ifneq ($(shell id -u), 0)
	@echo "You must be root to perform this action."
else
	@echo "copying $(SOFTNAME) to $(PREFIX)/bin/$(SOFTNAME)"
	cp $(SOFTNAME) $(PREFIX)/bin/$(SOFTNAME)
	@echo "making $(SOFTNAME) executable"
	chmod +x $(PREFIX)/bin/$(SOFTNAME)
endif

uninstall:
ifneq ($(shell id -u), 0)
	@echo "You must be root to perform this action."
else
	@echo "removing $(PREFIX)/bin/$(SOFTNAME)"
	rm $(PREFIX)/bin/$(SOFTNAME)
endif

update:
ifneq ($(shell id -u), 0)
	@echo "updating sources..."
	git pull --rebase
	@echo "Done."
else
	@echo "You must not be root to perform this action."
endif

help:
	@echo "usage: make [ install | uninstall | update | help ]"

# Fallbacks to make things easier
all: install
full: install
remove: uninstall
