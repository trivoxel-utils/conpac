# Consolidated Package Controller

```
 ________  ________  _____   __  _______  ________  ________
|   __   ||   __   ||     \ |  ||   __  ||   __   ||   __   |
|  |  '--'|  |  |  ||  |\  \|  ||  !__! ||  !__!  ||  |  '--'
|  !__.--||  !__!  ||  | \  '  ||  .----'|  .--.  ||  !__.--.
!________!!________!!__!  \____!!__!     !__!  !__!!________!
```

A feature-rich, cross-platform, simplified package managing program. It acts as a high-level wrapper that is capable of downloading and installing software and updates for many operating systems and offers many extra tools. When you interface with CONPAC, it pipes the commands through to the detected system package manager and automatically configures all package managers to behave the same way. It's an extremely simple, robust piece of software that doesn't require any extra software to run, making it easily portable to any system.

## Updates

**0.2 BETA:** Fixed bugs, added an option to manually configure CONPAC to use a specific package manager, updated the version and help screens.

## Sections:
* [Supported Platforms](#supported-platforms)
* [How to use](#how-to-use)
* [How to install](#how-to-install)
* [Updating](#updating)
* [How to remove](#how-to-remove)

## Supported platforms

CONPAC enables you to use the same commands, regardless of platform, to manage your packages. CONPAC currently supports a unified package management experience on the following platforms:

* Linux
	* [Arch Linux](https://archlinux.org) systems via [`pacman`](https://wiki.archlinux.org/title/Pacman)
	* [Fedora](https://fedoraproject.org) systems via [`dnf`](https://fedoraproject.org/wiki/DNF)
	* [OpenSUSE](https://opensuse.org) systems via [`zypper`](https://en.opensuse.org/Portal:Zypper)
	* [Debian](https://debian.org)/[Ubuntu](https://ubuntu.com) systems via [`apt`](https://wiki.debian.org/Apt)
	* _More will eventually be supported_
* BSD UNIX
	* [FreeBSD](https://freebsd.org) systems via [`pkg`](https://wiki.freebsd.org/pkg)
	* [Apple's macOS](https://apple.com/macos) via [`homebrew`](https://brew.sh)
	* _More will eventually be supported_

## How to use

```
usage: conpac <operation> [...]
operations:
  conpac {-c clean}         <packages>   deletes orphaned and selected packages
  conpac {-d delete}        <packages>   deletes software
  conpac {-g upgrade}                    upgrades software
  conpac {-h --help help}                shows this dialogue
  conpac {-i install}       <packages>   installs software
  conpac {-s search}        <packages>   searches repositories for software
  conpac {-u update}                     updates software
  conpac {-v version}                    shows program version
  conpac {   config}                     configure program settings
  conpac {-a:i -a:install}  <packages>   automates software installation
  conpac {-a:d -a:delete}   <packages>   automates software deletion
  conpac {-a:u -a:update}                automates software updates
  conpac {-a:g -a:upgrade}               automates software upgrades
  conpac {-d:repo}          <repository> deletes repository
  conpac {-i:repo}          <repository> installs repository
  conpac {-v:repo}          <repository> show repository information
```

## How to install

Step 1: Get the sources

```
mkdir ~/Sources
cd ~/Sources
git clone https://gitlab.com/trivoxel/conpac.git
cd conpac/
```
> **Note:** _You don't need to create a "Sources" folder. You can download the source code anywhere. I am using it as an example_

Step 2: Install CONPAC

```
sudo make install
```

Step 3: Test if your installation was successful

```
conpac -v
```

Alternatively, install with only one command:

```
git clone https://gitlab.com/trivoxel/conpac.git ~/ && cd ~/conpac && sudo make install
conpac -v
```

## Updating

From the git folder, run:

```
make update
sudo make install
```

## How to remove
From the git folder, run:

```
sudo make uninstall
```
